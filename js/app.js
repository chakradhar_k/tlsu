/**
 * Created by chakradhar.katta on 6/28/2017.
 */

var myApp = angular.module('TLSApp', ['ui.router','lbServices', 'angular-loading-bar', 'ngAnimate','ui.bootstrap','ui.select2','imageupload']);

myApp.config(function($stateProvider, $urlRouterProvider) {
    var searchState = {
        name: 'search',
        url: '/search',
        templateUrl: 'search.html',
        controller: 'LienController'
    }

    var createState = {
        name: 'create',
        url: '/create',
        templateUrl: 'create.html',
        controller: 'LienController'
    }

    var authState = {
        name: 'auth',
        url: '/auth',
        templateUrl: 'authorization.html'
    }

    var loginState = {
        name: 'login',
        parent: 'auth',
        url: '/login',
        templateUrl: 'login.html',
        controller: 'AuthController'
    }

    var registerState = {
        name: 'register',
        parent: 'auth',
        url: '/register',
        templateUrl: 'register.html',
        controller: 'AuthController'
    }

    $stateProvider.state(searchState);
    $stateProvider.state(createState);
    $stateProvider.state(authState);
    $stateProvider.state(loginState);
    $stateProvider.state(registerState);

    $urlRouterProvider.otherwise("/auth/login");
}).config(function(LoopBackResourceProvider) {

    // Use a custom auth header instead of the default 'Authorization'
    LoopBackResourceProvider.setAuthHeader('X-Access-Token');

    // Change the URL where to access the LoopBack REST API server
    // LoopBackResourceProvider.setUrlBase('http://10.10.53.195:3000/api');
    LoopBackResourceProvider.setUrlBase('http://127.0.0.1:3000/api');
});

myApp.controller('LienController',function($scope, $http, Document, $state){

    $scope.documentList = [];

    $scope.searchKey = "";
    $scope.searchLiens = function(){

        Document.find({filter:{where:{or:[{Last:{like:$scope.searchKey.toLocaleUpperCase()}},{First:{like:$scope.searchKey.toLocaleUpperCase()}}]},limit:10,include:{'lien':'documents'}}}).$promise
            .then(function(documentList) {
                $scope.documentList = documentList;
                console.log($scope.documentList);
            });


    }

    $scope.logout = function () {
        $state.go('login');
    }
    
    $scope.multiple = function (imageFiles) {
        // console.log(imageFiles);
        // console.log(JSON.stringify(imageFiles));
        $scope.images = [];

        for(var im in imageFiles) {

            console.log(im);
            var imageFile = imageFiles[im];
            $scope.images.push({"url":imageFile["url"]});

        }

        // $scope.images = imageFiles;
    }

    $scope.createLean = function () {

        $scope.document["Last"] = $scope.document["Last"].toString().toLocaleUpperCase();
        $scope.document["First"] = $scope.document["First"].toString().toLocaleUpperCase();
        $scope.document["type"] = "internal";
        $scope.document["content"] = JSON.parse(document.getElementById("result").value);
        $scope.document["status"] = "pending";
        $scope.document["images"] = $scope.images;
        Document.create($scope.document).$promise
            .then(function(cUser) {
                console.log(cUser);
                $state.go('search');
            });

    }

    $scope.validateDocument = function(document) {

        document["status"] = "validated";
        Document.updateAttributes(
            {id:    document.id},
            {status: document.status}
        );

    }

    $scope.moveToCreate = function() {
        $state.go('create');
    }

    $scope.moveToSearch = function() {
        $state.go('search');
    }

});

myApp.controller('AuthController',function($scope, $http, User, $state){


    $scope.register = function () {
        User.create($scope.user).$promise
            .then(function(cUser) {
                console.log(cUser);
                $state.go('search');
            });
    }

    $scope.login = function () {
        User.login($scope.user).$promise
            .then(function(cUser) {
                console.log(cUser);
                $state.go('search');
            });
    }

    $scope.toRegister = function () {
        $state.go('register');
    }

    $scope.toLogin = function () {
        $state.go('login');
    }

});

